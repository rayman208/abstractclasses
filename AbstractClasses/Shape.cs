﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClasses
{
    abstract class Shape
    {
        protected int x, y;
        protected string color;

        protected Shape(int x, int y, string color)
        {
            this.x = x;
            this.y = y;
            this.color = color;
        }

        public abstract void Draw();

        public virtual void MyMethod()
        {
            Console.WriteLine("MyMethod");
        }
    }
}
