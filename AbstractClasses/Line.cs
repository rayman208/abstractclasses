﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClasses
{
    class Line:Shape
    {
        private int x2, y2;

        public Line(int x1, int y1, int x2, int y2, string color) : base(x1, y1, color)
        {
            this.x2 = x2;
            this.y2 = y2;
        }

        public override void Draw()
        {
            Console.WriteLine($"LINE: x1:{x} y1:{y} x2:{x2} y2:{y2} color:{color}");
        }

        public override void MyMethod()
        {
            Console.WriteLine("Line MyMethod");
        }
    }
}
