﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClasses
{
    class Circle:Shape
    {
        private int r;

        public Circle(int x, int y, int r, string color) : base(x, y, color)
        {
            this.r = r;
        }

        public override void Draw()
        {
            Console.WriteLine($"CIRCLE: x:{x} y:{y} r:{r} color:{color}");
        }
    }
}
