﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClasses
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape[] shapes = new Shape[4];

            shapes[0] = new Square(1,1,2,5,"Red");
            shapes[1] = new Circle(2,3,1,"Yellow");
            shapes[2] = new Line(1,1,2,2,"Black");
            shapes[3] = new Triangle(1, 1, 2, "Black");

            for (int i = 0; i < shapes.Length; i++)
            {
                shapes[i].Draw();
                shapes[i].MyMethod();

                Console.WriteLine();
            }

            Console.ReadKey();
        }
    }
}
