﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClasses
{
    class  Square:Shape
    {
        private int w, h; 

        public Square(int x, int y, int w, int h, string color) : base(x, y, color)
        {
            this.w = w;
            this.h = h;
        }

        public override void Draw()
        {
            Console.WriteLine($"SQUARE: x:{x} y:{y} w:{w} h:{h} color:{color}");
        }
    }
}
