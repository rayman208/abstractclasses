﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClasses
{
    class Triangle:Shape
    {
        private int a;

        public Triangle(int x, int y, int a, string color) : base(x, y, color)
        {
            this.a = a;
        }

        public override void Draw()
        {
            Console.WriteLine($"TRIANGLE: x:{x} y:{y} a:{a} color:{color}");
        }
    }
}
